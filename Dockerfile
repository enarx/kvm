FROM registry.gitlab.com/enarx/lab:latest

# Install and configure the kernel
RUN apt update \
 && apt install --no-install-recommends -y initramfs-tools \
 && ln -sf /bin/true /usr/sbin/update-initramfs \
 && apt install --no-install-recommends -y linux-image-generic \
 && rm -rf /var/lib/apt/lists/*
RUN cd /boot; ln -s vmlinuz* wyrcan.kernel

# Add udev rules
RUN groupadd -r sgx_prv
COPY 99-sgx.rules /etc/udev/rules.d/
